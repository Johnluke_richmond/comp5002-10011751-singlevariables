﻿using System;

namespace comp5002_10011751_singlevariables
{
    class Program
    {
        static void Main(string[] args)
        {
            var name = "waddl_er";
            Console.WriteLine($"Hello {name}!");
            Console.ReadLine();
        }
    }
}
